package com.demo.mbdemo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * DemoPO
 *
 * @author : stan
 * @version : 1.0
 * @date :  2022/8/21 1:39 AM
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_user")
public class DemoPO extends Model<DemoPO> implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;
    @TableField("user_name")
    private String userName;

    public static final String USER_ID = "user_id";

    public static final String USER_NAME = "user_name";

    @Override
    protected Serializable pkVal() {
        return this.userId;
    }
}
