package com.demo.mbdemo.service;

import com.demo.mbdemo.dao.DemoMapper;
import com.demo.mbdemo.dao.MPDemoMapper;
import com.demo.mbdemo.po.DemoPO;
import com.demo.mbdemo.router.DemoRouter;
import com.stanwind.dbselector.anno.DBNode;
import com.stanwind.dbselector.anno.NodeType;
import com.stanwind.dbselector.aop.NodeContext;
import javafx.application.Application;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * DemoService
 *
 * @author : stan
 * @version : 1.0
 * @date :  2022/8/21 1:50 AM
 **/
@Service
@Slf4j
public class DemoService {
    @Autowired
    private DemoMapper demoMapper;

    @Autowired
    private MPDemoMapper mpDemoMapper;

    @Autowired
    private ApplicationContext context;

    @DBNode(type = NodeType.MASTER)
    public void t1() {
        log.info("force master");
        demoMapper.selectById(1L);
    }

    @DBNode(type = NodeType.SLAVE)
    public void t2() {
        log.info("force_slave");
        mpDemoMapper.selectById(1L);
    }

    @DBNode(type = NodeType.ASSIGN, node = "node1")
    public void t3() {
        log.info("assign node1");
        mpDemoMapper.selectById(1L);
    }

    public void t33() {
        DemoService service = context.getBean(DemoService.class);
        for (long i = 1; i < 5; i++) {
            NodeContext.getContext().addObject(DemoPO.USER_ID, i);
            service.t333(i);
        }
    }

    @DBNode(type = NodeType.ASSIGN, router = DemoRouter.class)
    public void t333(Long id) {
        log.info("assign router id {}", id);
        mpDemoMapper.selectById(id);
    }

    @DBNode(type = NodeType.CUSTOM, node = "hahahahahah")
    public void t4() {
        log.info("custom hint");
        mpDemoMapper.selectById(1L);
    }

    @DBNode(type = NodeType.IGNORE)
    public void t5() {
        log.info("none");
        mpDemoMapper.selectById(1L);
    }


}
