package com.demo.mbdemo.router;

import com.demo.mbdemo.po.DemoPO;
import com.stanwind.dbselector.aop.NodeContext;
import com.stanwind.dbselector.route.NodeRouter;

import java.util.ArrayList;
import java.util.List;

/**
 * DemoRouter
 * 轮询路由
 *
 * @author : stan
 * @version : 1.0
 * @date :  2022/8/21 2:22 AM
 **/
public class DemoRouter implements NodeRouter {

    public static final List<String> nodes = new ArrayList<>();

    static {
        nodes.add("round_node1");
        nodes.add("round_node2");
        nodes.add("round_node3");
    }

    @Override
    public String route() {
        Long id = NodeContext.getContext().getObject(DemoPO.USER_ID, Long.class);
        long index = id % nodes.size();

        return nodes.get((int) index);
    }
}
