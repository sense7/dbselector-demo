package com.demo.mbdemo.dao;

import com.demo.mbdemo.po.DemoPO;
import org.springframework.stereotype.Repository;

/**
 * DemoMapper
 *
 * @author : stan
 * @version : 1.0
 * @date :  2022/8/21 1:40 AM
 **/
public interface DemoMapper {
    DemoPO selectById(Long id);
}
