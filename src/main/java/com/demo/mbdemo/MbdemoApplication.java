package com.demo.mbdemo;

import com.demo.mbdemo.service.DemoService;
import com.stanwind.dbselector.anno.EnableDBSelector;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableDBSelector
@MapperScan("com.demo.mbdemo.dao")
@EnableAspectJAutoProxy(exposeProxy = true)
public class MbdemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(MbdemoApplication.class, args);

    }

}
