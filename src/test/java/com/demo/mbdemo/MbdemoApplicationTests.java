package com.demo.mbdemo;

import com.demo.mbdemo.service.DemoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MbdemoApplicationTests {

    @Autowired
    private DemoService demoService;

    @Test
    void contextLoads() {
        demoService.t1();
        demoService.t2();
        demoService.t3();
        demoService.t33();
        demoService.t4();
        demoService.t5();
    }

}
